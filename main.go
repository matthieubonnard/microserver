package main

import (
	"io"
	"io/ioutil"
	"flag"
	"fmt"
	"net"
	"os"
	"os/exec"
	"log"
)

var (
	Trace   *log.Logger
	Info    *log.Logger
	Warning *log.Logger
	Error   *log.Logger
)

func Init(traceHandle io.Writer, infoHandle io.Writer, warningHandle io.Writer, errorHandle io.Writer) {
	Trace = log.New(traceHandle, "TRACE: ", log.Ldate|log.Ltime)
	Info = log.New(infoHandle, "INFO: ", log.Ldate|log.Ltime)
	Warning = log.New(warningHandle, "WARNING: ", log.Ldate|log.Ltime)
	Error = log.New(errorHandle, "ERROR: ", log.Ldate|log.Ltime)
}

func main() {
	Init(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)

	// Usage Function
	flag.Usage = func() {
		fmt.Printf("Usage of %s:\n", os.Args[0])
		fmt.Printf("    microserver [-i IP_ADDR] [-t (TCP|UDP)] [-p PORT] [-c COMMAND]\n")
		flag.PrintDefaults()
	}

	// Define program parameters
	interfacePtr := flag.String("i", "0.0.0.0", "The listening server interface")
	typePtr := flag.String("t", "tcp", "TCP or UDP")
	portPtr := flag.String("p", "3333", "The listening server port")
	cmdPtr := flag.String("c", "uptime", "The command to execute")
	flag.Parse()

	// Listen for incoming connections.
	l, err := net.Listen(*typePtr, *interfacePtr+":"+*portPtr)
	if err != nil {
		Error.Println("Error listening : ", err.Error())
		os.Exit(1)
	}
	// Close the listener when the application closes.
	defer l.Close()
	Info.Println("Server listening on " + *typePtr + "://" + *interfacePtr + ":" + *portPtr)
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			Info.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn, cmdPtr)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn, cmdPtr *string ) {
	// Execute the specified OS command and return the output
	out, _ := exec.Command("sh","-c",*cmdPtr).Output()
	// Send output in connection
	conn.Write([]byte(out))
	// Close the connection when you're done with it.
	conn.Close()
}
