# Introduction

Microserver is a extra-lightweight TCP or UDP server that return the output of the specified command.

# Usage
### General

```bash
microserver [-i IP_ADDR] [-t (TCP|UDP)] [-p PORT] [-c COMMAND]
```

### Example

The command "vmstat 1 2 | tail -n +4 | awk '{ print $15 }'" return the %idle of the system.
This case can be use like HA Proxy agent-check to dynamicaly re-weight backend :

```bash
microserver -i 0.0.0.0 -p 5555 -c "vmstat 1 2 | tail -n +4 | awk '{ print $15 }'"
```